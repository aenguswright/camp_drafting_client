// Configuration for your app
const path = require('path')

module.exports = function (ctx) {
  return {
    boot: [
      'day',
      'lodash',
      'util',
      'i18n',
      'stomp',
      'apollo',
      'auth',
      'resultResender'
    ],
    css: [
      'app.styl'
    ],
    extras: [
      'material-icons'
    ],
    build: {
      scopeHoisting: true,
      vueRouterMode: 'history',
      // vueCompiler: true,
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      extendWebpack (cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules|quasar)/
        })
        cfg.module.rules.push({
          test: /\.(graphql|gql)$/,
          exclude: /node_modules/,
          loader: 'graphql-tag/loader'
        })
        cfg.resolve.alias = {
          ...cfg.resolve.alias,

          Components: path.resolve(__dirname, './src/components'),
          Themes: path.resolve(__dirname, './src/themes'),
          Queries: path.resolve(__dirname, './src/graphql/queries'),
          Mutations: path.resolve(__dirname, './src/graphql/mutations'),
          Assets: path.resolve(__dirname, './src/assets')
        }
      },
      env: {
        'API_URL': 'https://spectatorjudgingdeva537db424.hana.ondemand.com/andromeda-1.0.0/',
        'MESSAGING_URL': 'wss://spectatorjudgingdeva537db424.hana.ondemand.com/andromeda-1.0.0/messaging',
        'FLAG_PROVIDER': 'https://s3.eu-central-1.amazonaws.com/spectator-judging/flags/',
        'DISPLAY_URL': 'https://cg.blackhorse-one.com/'
        // API_URL: ctx.dev ? 'http://localhost:8080/' : 'https://andromeda-insightful-koala.cfapps.eu10.hana.ondemand.com/', // 'https://spectatorjudginga14295f70.hana.ondemand.com/andromeda-1.0.0/',
        // MESSAGING_URL: ctx.dev ? 'ws://localhost:8080/messaging' : 'wss://andromeda-insightful-koala.cfapps.eu10.hana.ondemand.com/messaging', // 'wss://spectatorjudginga14295f70.hana.ondemand.com/andromeda-1.0.0/messaging',
        // FLAG_PROVIDER: 'https://s3.eu-central-1.amazonaws.com/spectator-judging/flags/'
      }
    },
    devServer: {
      // https: true,
      port: 8082,
      open: true // opens browser window automatically
    },
    // framework: 'all' --- includes everything; for dev only!
    framework: {
      cssAddon: true,
      components: [
        'QAvatar',
        'QColor',
        'QLayout',
        'QHeader',
        'QPageContainer',
        'QPage',
        'QPagination',
        'QTooltip',
        'QToolbar',
        'QToolbarTitle',
        'QBtn',
        'QIcon',
        'QInnerLoading',
        'QList',
        'QDialog',
        'QItem',
        'QItemLabel',
        'QItemSection',
        'QLinearProgress',
        'QSeparator',
        'QSpinnerRadio',
        'QSelect'
      ],
      directives: [
        'Ripple'
      ],
      // Quasar plugins
      plugins: [
        'Notify'
      ]
      // iconSet: 'material-icons'
      // lan: 'de' // Quasar language
    },
    // animations: 'all' --- includes all animations
    animations: [
      'SlideInUp'
    ],
    ssr: {
      pwa: true
    },
    pwa: {
      cacheExt: 'js,html,css,ttf,eot,otf,woff,woff2,json,svg,gif,jpg,jpeg,png,wav,ogg,webm,flac,aac,mp4,mp3',
      manifest: {
        name: 'eCampdrafting',
        short_name: 'eCampdrafting',
        description: 'Integrated Electronic Scoring for Campdrafting. Part of the "E Series" of equestrian scoring software.',
        display: 'standalone',
        orientation: 'landscape',
        background_color: '#111111',
        theme_color: '#95292d',
        scope: '/',
        icons: [
          {
            'src': 'icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },
    cordova: {
      // id: 'org.cordova.quasar.app'
    },
    electron: {
      // bundler: 'builder', // or 'packager'
      extendWebpack (cfg) {
        // do something with Electron process Webpack cfg
      },
      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Window only
        // win32metadata: { ... }
      },
      builder: {
        // https://www.electron.build/configuration/configuration

        // appId: 'quasar-app'
      }
    }
  }
}
