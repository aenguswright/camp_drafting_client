function load (component) {
  return () => import(`pages/${component}.vue`)
}
// function componentLoad (component) {
//   return () => import(`components/${component}.vue`)
// }

// const withPrefix = (prefix, routes) =>
//   routes.map((route) => {
//     route.path = prefix + '/' + route.path
//     return route
//   })

const routes = [
  {
    path: '/',
    component: () => import('layouts/Basic.vue'),
    redirect: '/login',
    children: [
      { path: '/login', component: load('Authorization/login') },
      { path: '/welcome', component: load('Core/welcome') },
      { path: '/competitions/:showId', component: load('CampDrafting/competitions') },
      { path: '/rounds/:showId/:competitionId', component: load('CampDrafting/rounds') },
      { path: '/competitors/:showId/:competitionId/:roundId', component: load('CampDrafting/roundEnvironment') }
    ]

  }
]

// Always leave this as last one
routes.push({
  path: '*',
  component: () => import('pages/Error404.vue')
})

export default routes
