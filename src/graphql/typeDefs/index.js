export const typeDefs = [
  require('../typeDefs/Nation.gql'),
  require('../typeDefs/Horse.gql'),
  require('../typeDefs/Athlete.gql'),
  require('../typeDefs/CampDraftingCompetitor.gql')
]
