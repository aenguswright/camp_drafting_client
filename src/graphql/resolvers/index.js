export const resolvers = {
  Mutation: {
    setHasNewVersion: (_, { hasNewVersion }, { cache }) => {
      const targetQuery = require('../queries/HasNewVersion.gql')
      const hasNewVersionData = cache.readQuery({ query: targetQuery })
      hasNewVersionData.hasNewVersion = hasNewVersion
      cache.writeQuery({ query: targetQuery, data: hasNewVersionData })
      return hasNewVersion
    }
  }
}
