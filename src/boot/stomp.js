import Stomp from './local/stomp'

// leave the export, even if you don't use it
export default ({ app, router, Vue }) => {
  Stomp.init()
  Vue.prototype.$stomp = Stomp
}
