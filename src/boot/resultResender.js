import ResultResender from './local/resultResender'

// "async" is optional
export default async ({ app, Vue }) => {
  if (Vue.prototype.$auth.isAuthenticated()) {
    Vue.prototype.$resultResender = new ResultResender(app.apolloProvider.defaultClient)
  }
}
