import Auth from './local/auth'

// "async" is optional
export default ({ Vue, app, router }) => {
  Vue.prototype.$auth = Auth
}

export { Auth }
