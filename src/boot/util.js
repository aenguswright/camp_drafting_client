import Util from './local/util'
import VModal from 'vue-js-modal'

// leave the export, even if you don't use it
export default ({ app, router, Vue }) => {
  Vue.prototype.$util = Util
  Vue.use(VModal)
}
