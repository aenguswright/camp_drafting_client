var Util = {}

Util.getFlagUrl = function (name) {
  if (name) {
    return process.env.FLAG_PROVIDER + 'svg/' + name.toLowerCase() + '.svg'
  }
  return this.getEmptyFlag()
}

Util.getEmptyFlag = function (event) {
  if (event) {
    event.target.src = 'unknown.png'
    return
  }
  return 'unknown.png'
}

Util.judgePositions = () => {
  return ['k', 'e', 'h', 'c', 'm', 'b', 'f']
}

Util.filteredJudgePositions = function (competitionJudges) {
  return this.judgePositions().filter((position) => {
    return competitionJudges[position] || false
  })
}

Util.getStatusAbbr = function (status, short = false) {
  if (typeof status === 'object') status = status.status
  switch (status) {
    case 'RETIRED': return short ? 'RT' : 'RET'
    case 'ELIMINATED': return short ? 'EL' : 'ELIM'
    case 'DID_NOT_COMPETE': return short ? 'WD' : 'WD'
    case 'NO_SHOW': return short ? 'NS' : 'NS'
    case 'DISQUALIFIED': return short ? 'DQ' : 'DSQ'
    default: return null
  }
}

Util.getFlagCode = function (competitor, competition, show, url = false) {
  if ((competition.event && !competition.event.feiId) &&
    competitor.athlete?.nation?.ioc.toUpperCase() === show.nation?.ioc.toUpperCase()) {
    if (competitor.athlete?.club) {
      switch (competitor.athlete.club.name) {
        case 'Equestrian Victoria':
          return url ? 'regions/aus-vic' : 'VIC'
        case 'Equestrian New South Wales':
          return url ? 'regions/aus-nsw' : 'NSW'
        case 'Northern Territory':
          return url ? 'regions/aus-nt' : 'NT'
        case 'Equestrian South Australia':
          return url ? 'regions/aus-sa' : 'SA'
        case 'Equestrian Western Australia':
          return url ? 'regions/aus-wa' : 'WA'
        case 'Equestrian Queensland':
          return url ? 'regions/aus-qld' : 'QLD'
        case 'Australian Capital Territory':
          return url ? 'regions/aus-act' : 'ACT'
        case 'Equestrian Tasmania':
          return url ? 'regions/aus-tas' : 'TAS'
        default:
          break
      }
    }
  }
  return competitor.athlete?.nation?.ioc
}
Util.sanitizeString = function (s) {
  if (!s) return ''
  return String(s).replace(/&/g, '&amp;').replace(/</g, '').replace(/"/g, '')
}

export default Util
