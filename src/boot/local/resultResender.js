import SaveCampDraftingJudgeResultMutation from 'Mutations/campdrafting/SaveCampDraftingJudgeResult.gql'
import _ from 'lodash'

export default class ResultResender {
  constructor (apolloClient) {
    this.apolloClient = apolloClient
    this.transmitting = false

    const untransmittedResults = JSON.parse(window.localStorage.getItem('untransmittedResults'))

    if (!untransmittedResults) {
      window.localStorage.setItem('untransmittedResults', JSON.stringify([]))
    }

    window.setInterval(this._resendResults.bind(this), 5000)
  }

  async _resendResults () {
    // only allow new requests if we got a response from all of the previous ones
    if (this.transmitting) return
    try {
      this.transmitting = true
      const untransmittedResults = JSON.parse(window.localStorage.getItem('untransmittedResults'))

      Promise.allSettled(untransmittedResults.map((judgeResult) => {
        delete judgeResult.__typename
        delete judgeResult.competitionJudge?.__typename

        return this.apolloClient.mutate({
          mutation: SaveCampDraftingJudgeResultMutation,
          variables: {
            judgeResult: judgeResult
          }
        })
      })).then((results) => {
        results.forEach(result => {
          if (result.status === 'fulfilled') {
            // Successful transmission, remove from results
            _.remove(untransmittedResults, (res) => {
              // eslint-disable-next-line eqeqeq
              return res.roundEntry.id == result.value.data.saveCampDraftingJudgeResult.roundEntry.id
            })
          }
        })
      }).finally(_ => {
        window.localStorage.setItem('untransmittedResults', JSON.stringify(untransmittedResults))
        this.transmitting = false
      })
    } catch (err) {
      console.error(err)
    }
  }
}
