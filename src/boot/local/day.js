'use strict'

/**
 * Module dependencies.
 * @private
 */

const dayjs = require('dayjs')
const advancedFormat = require('dayjs/plugin/advancedFormat')

dayjs.extend(advancedFormat)

export default {
  /**
    * Vue
    * @param {Vue} Vue
  */
  install (Vue) {
    Object.defineProperties(Vue.prototype, {
      $dayjs: {
        get () {
          return dayjs
        }
      },
      $date: {
        get () {
          return dayjs
        }
      }
    })
    Vue.dayjs = dayjs
  }
}
