
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

import RefreshJWTQuery from 'Queries/RefreshJWT.gql'
import AuthenticateQuery from 'Queries/Authenticate.gql'
import AuthenticateWithTokenQuery from 'Queries/AuthenticateWithToken.gql'
import VerifyUnverifiedJudgeByMail from 'Mutations/judge/VerifyUnverifiedJudgeByMail.gql'

import jwtDecode from 'jwt-decode'

var Auth = {}
Auth.onLogout = null

// Create the apollo client
const link = createHttpLink({
  uri: process.env.API_URL + 'api/graph'
})
const cache = new InMemoryCache()
const apolloClient = new ApolloClient({
  cache, link
})

Auth.isAuthenticated = function () {
  const authResult = JSON.parse(window.localStorage.getItem('authResult'))
  return !!(authResult && authResult.jwt)
}

Auth.getUser = function () {
  const authResult = JSON.parse(window.localStorage.getItem('authResult'))
  return authResult.account
}

Auth.logout = function () {
  window.localStorage.removeItem('authResult')
  window.localStorage.removeItem('isJudge')
  if (this.onLogout) this.onLogout()
  window.location.href = '/login'
}

Auth.authenticateWithToken = function (token, platform) {
  return apolloClient.query({
    query: AuthenticateWithTokenQuery,
    variables: {
      token: token,
      platform: platform
    },
    fetchPolicy: 'no-cache'
  })
}

Auth.authenticate = function (email, password, platform) {
  return apolloClient.query({
    query: AuthenticateQuery,
    variables: { email: email, password: password, platform: platform },
    fetchPolicy: 'no-cache'
  })
}

Auth.verifyAccount = function (token) {
  return apolloClient.query({
    query: VerifyUnverifiedJudgeByMail,
    variables: {
      token: token
    },
    fetchPolicy: 'no-cache'
  })
}

Auth.getIdToken = async function () {
  const authResult = JSON.parse(window.localStorage.getItem('authResult'))
  if (!(authResult && authResult.jwt)) this.logout()
  if (!this._isTokenValid(authResult.jwt)) {
    // Only for backwards-compatibility. Can be removed in the future
    const account = authResult.user ? authResult.user : authResult.account
    var token = await this._refreshToken(account.refreshToken, authResult.jwt)
    if (token !== authResult.jwt) {
      authResult.jwt = token
      window.localStorage.setItem('authResult', JSON.stringify(authResult))
    }
  }
  return authResult.jwt
}

Auth._refreshToken = async function (refreshToken, oldJwt) {
  try {
    const response = await apolloClient.query({
      query: RefreshJWTQuery,
      variables: {
        refreshToken: refreshToken
      },
      fetchPolicy: 'no-cache'
    })
    return response.data.refreshJWT.jwt
  } catch ({ networkError }) {
    if (!networkError) {
      this.logout()
    }
    return oldJwt
  }
}

Auth._isTokenValid = function (jwt) {
  var oDecodedToken = jwtDecode(jwt)
  var currentTime = Date.now() / 1000
  return oDecodedToken.exp >= (currentTime + 300)
}

export default Auth
