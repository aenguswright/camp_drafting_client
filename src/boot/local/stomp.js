import { Client } from '@stomp/stompjs'
import { Auth } from 'boot/auth'

var Stomp = {}

Stomp._client = new Client()
Stomp._currentConnectIndex = 0
Stomp._connectSubscriptions = {}
Stomp._currentDisconnectIndex = 0
Stomp._disconnectSubscriptions = {}

Stomp.connect = function () {
  this._client.activate()
}

Stomp._onConnect = function () {
  console.log('[STOMP] connected')
  Object.keys(Stomp._connectSubscriptions).forEach(function (key) {
    Stomp._connectSubscriptions[key]()
  })
}

Stomp._onDisconnect = function () {
  console.log('[STOMP] disconnected')
}

Stomp._onError = function (errorFrame) {
  console.log('[STOMP] encountered an error ' + errorFrame)
}

Stomp._onDebug = function (debugStr) {
  console.log(debugStr)
}

Stomp._beforeConnect = async function () {
  await Auth.getIdToken().then((token) => {
    Stomp._client.connectHeaders = {
      'Authorization': 'Bearer ' + token,
      'host': 'AH'
    }
  })
}

Stomp._client.onConnect = Stomp._onConnect
Stomp._client.onDisconnect = Stomp._onDisconnect
Stomp._client.onStompError = Stomp._onError
Stomp._client.beforeConnect = Stomp._beforeConnect
if (process.env.DEV) {
  Stomp._client.logRawCommunication = true
  Stomp._client.debug = Stomp._onDebug
}

Stomp.send = function (path, message) {
  if (this._client.connected) {
    this._client.publish({destination: path, body: message})
  }
}

Stomp.disconnect = function () {
  Object.keys(Stomp._disconnectSubscriptions).forEach(function (key) {
    Stomp._disconnectSubscriptions[key]()
  })
  this._client.deactivate()
}

Stomp.init = function () {
  this._client.brokerURL = process.env.MESSAGING_URL
  this._client.reconnectDelay = 5000
  this._client.heartbeatIncoming = 0
  this._client.heartbeatOutgoing = 0
  if (Auth.isAuthenticated()) {
    this.connect()
  }
}

Stomp.reconnect = function () {
  this.disconnect()
  this.connect()
}

Stomp.subscribe = function (path, callback) {
  if (this._client.connected) {
    return this._client.subscribe(path, callback)
  }
}

Stomp.subscribeToConnect = function (callback) {
  this._connectSubscriptions[this._currentConnectIndex] = callback
  if (this._client.connected) {
    callback()
  }
  return this._currentConnectIndex++
}

Stomp.unsubscribeToConnect = function (index) {
  delete this._connectSubscriptions[index]
}

Stomp.subscribeToDisconnect = function (callback) {
  this._disconnectSubscriptions[this._currentDisconnectIndex] = callback
  if (!this._client.connected) {
    callback()
  }
  return this._currentDisconnectIndex++
}

Stomp.unsubscribeToDisconnect = function (index) {
  delete this._disconnectSubscriptions[index]
}

export default Stomp
