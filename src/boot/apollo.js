import 'isomorphic-unfetch'
import { Quasar } from 'quasar'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache, defaultDataIdFromObject, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory'
import VueApollo from 'vue-apollo'
import { from } from 'apollo-link'
import { setContext } from 'apollo-link-context'
import { typeDefs } from '../graphql/typeDefs/index.js'
import { resolvers } from '../graphql/resolvers/index.js'
import { BatchHttpLink } from 'apollo-link-batch-http'
import fragmentTypes from '../graphql/fragmentTypes.json'
import { toIdValue } from 'apollo-utilities'

import { Auth } from 'boot/auth'

// "async" is optional
export default async ({ app, Vue }) => {
  var userSettings = JSON.parse(window.localStorage.getItem('userSettings'))

  userSettings = userSettings || {
    __typename: 'UserSettings',
    locale: Quasar.lang.getLocale().split('-')[0] || 'en'
  }

  const httpLink = new BatchHttpLink({
    uri: process.env.API_URL + 'api/graph',
    headers: {
      'Accept-Language': Quasar.lang.getLocale().split('-')[0] || 'en'
    },
    responseTransformer: async response => response.json().then(({ data }) => data)
  })

  const asyncAuthLink = setContext((_, { headers }) => {
    return Auth.getIdToken().then(token => {
      if (!token) {
        return { headers }
      }
      return {
        headers: {
          ...headers,
          'Authorization': 'Bearer ' + token
        }
      }
    })
  })

  const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData: fragmentTypes
  })

  const cache = new InMemoryCache({
    fragmentMatcher: fragmentMatcher,
    cacheRedirects: {
      Query: {
        publicShow: (_, args) => toIdValue(defaultDataIdFromObject({ __typename: 'Show', id: args.id }))
      }
    }
  })

  cache.writeData({
    data: {
      authResult: JSON.parse(window.localStorage.getItem('authResult')),
      userSettings: userSettings,
      hasNewVersion: false
    }
  })

  const proxy = new ApolloClient({
    link: from([
      asyncAuthLink,
      httpLink
    ]),
    cache,
    queryDeduplication: true,
    typeDefs,
    resolvers
  })

  const apolloProvider = new VueApollo({
    clients: {
      proxy
    },
    defaultClient: proxy,
    errorHandler ({ graphQLErrors, networkError }) {
      if (graphQLErrors) {
        graphQLErrors.map(({ message, locations, path }) =>
          console.error(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
          )
        )
      }
      if (networkError) {
        console.error(`[Network error]: ${networkError}`)
      }
    }
  })

  Vue.use(VueApollo)
  app.apolloProvider = apolloProvider
  Auth.onLogout = () => {
    apolloProvider.defaultClient.stop()
    apolloProvider.defaultClient.cache.reset()
    apolloProvider.defaultClient.clearStore()
  }
}
