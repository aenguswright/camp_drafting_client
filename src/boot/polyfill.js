const polyfill = require('es7-object-polyfill')

// "async" is optional
export default async ({ Vue }) => {
  Vue.use(polyfill)
}
