
export default async ({ _, Vue }) => {
  Vue.filter('round', function (value) {
    if (!value) return ''
    return value.toFixed(2)
  })
}
