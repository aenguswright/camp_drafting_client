import VersionChecker from './local/versionChecker'

// "async" is optional
export default async ({ app, Vue }) => {
  Vue.prototype.$versionChecker = new VersionChecker(app.apolloProvider.defaultClient)
}
