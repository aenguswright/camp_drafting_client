import VueDayJS from './local/day'
// leave the export, even if you don't use it
export default ({ app, router, Vue }) => {
  Vue.use(VueDayJS)
}
