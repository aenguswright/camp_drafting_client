import VueI18n from 'vue-i18n'
import messages from 'src/i18n'
import { Quasar } from 'quasar'

export default async ({ app, Vue }) => {
  Vue.use(VueI18n)

  // merge in theme translations
  let fallbackLocale = 'en'
  // const themeIdentifier = 'master'
  // eslint-disable-next-line no-unused-vars
  // let themedMessages = messages
  let locale = (localStorage.getItem('locale') || Quasar.lang.getLocale()).split('-')[0]
  // if (themeIdentifier) {
  //   const themeLocalization = require('../themes/' + themeIdentifier + '.json').localization
  //   if (themeLocalization) {
  //     // themedMessages = merge(themedMessages, themeLocalization.i18n)
  //     fallbackLocale = themeLocalization.fallbackLocale
  //     if (themeLocalization.forceLocale) {
  //       locale = themeLocalization.forceLocale
  //     }
  //   }
  // }

  // Set i18n instance on app
  app.i18n = new VueI18n({
    locale: locale,
    fallbackLocale: fallbackLocale,
    messages
  })
}
