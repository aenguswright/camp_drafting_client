import enUS from './en'
import deDE from './de'
import nlNL from './nl'
import ruRU from './ru'

export default {
  'en': enUS,
  'de': deDE,
  'nl': nlNL,
  'ru': ruRU
}
